require "rails_helper"

feature "create post" do
    scenario "successfully" do
        visit root_path
        click_on "New post"
        fill_in "Title", with: "test title 1"
        fill_in "Content", with: "test content 1"
        click_on "Save Post"

        expect(page).to have_css 'td', text: "test title 1" 
    end
end

feature "update post" do
    scenario "successfully" do
        visit root_path
        click_on "Edit"
        fill_in "Title", with: "test title 1-1"
        fill_in "Content", with: "test content 1-1"
        click_on "Update Post"

        expect(page).to have_css 'td', text: "test title 1-1"
    end
end

feature "delete post" do
    scenario "sucessfully" do
        visit root_path
        click_on "Destroy"
        click_on "确定"
        expect(page).not_to have_css 'td', text: "test title 1-1"
    end
end