require "rails_helper"

feature "homepage title" do
    scenario "successfully" do
        visit root_path
        expect(page).to have_css "h1", text: "My Blog"
        expect(page).to have_css "a", text: "New post"
    end
end